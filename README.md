# OpenShift Project Setup Playbook

This Ansible playbook automates the process of creating a new OpenShift project, adding a service account to the project, assigning the edit role to the service account, and extracting the token for the service account.

### Prerequisites

- Ansible installed.
- OpenShift command-line tool (`oc`).
- A configured connection to an OpenShift cluster, either by setting up a kubeconfig or using other methods.
- Install the `kubernetes.core` Ansible collection:

  ```bash
  ansible-galaxy collection install kubernetes.core
  ```

- Install the Python Kubernetes client:

  ```bash
  pip3 install kubernetes
  ```

### Directory Structure

```
.
├── openshift_playbook.yml
└── vars
    └── vars.yml
```

### Setup

1. **Clone the Repository**:

```bash
git clone <repository_url>
cd <repository_directory_name>
```

2. **Configure the Variables**:

Edit the `vars/vars.yml` file to include your specific configuration.

```yaml
project_name: "your_project_name"
service_account_name: "your_service_account_name"
```

Replace `your_project_name` and `your_service_account_name` with appropriate values for your environment.

### Execution

Navigate to the directory containing the playbook and execute:

```bash
ansible-playbook -e @vars/vars.yml openshift_playbook.yml
```

#### Execute Specific Scenarios

1. **Only Create a Project**:

   ```bash
   ansible-playbook -e @vars/vars.yml openshift_playbook.yml --tags "create_project"
   ```

2. **Only Create a Service Account, Add Edit Role, and Extract Token** (for an existing project):

   ```bash
   ansible-playbook -e @vars/vars.yml openshift_playbook.yml --tags "create_service_account,add_edit_role,describe_secret"
   ```

### Dry Run

Before making actual changes, you can perform a dry run:

```bash
ansible-playbook -e @vars/vars.yml openshift_playbook.yml --check
```

This will show the actions that Ansible will take without actually making any changes.

### Troubleshooting

If you encounter any issues:
- Ensure the OpenShift command-line tool (`oc`) is installed and accessible.
- Ensure you have the required permissions on the OpenShift cluster.
- Make sure the variables in `vars/vars.yml` are correctly set.

---

The added "Execute Specific Scenarios" section provides the commands to execute specific tasks based on their tags. Adjust any details as necessary to better match your environment or workflow.
